# PHP, PDO, QUERY (not prepare), AJAX, JQUERY

### Base de données à créer !

- **db_name** : \_pdoTEST
- **table** : users

|     | NAME        | TYPE           | DEFAULT           |
| --- | ----------- | -------------- | ----------------- |
|     | `id`        | 'int(11)'      |
|     | `pseudo`    | 'text'         |
|     | `mdp`       | 'varchar(255)' |
|     | `email`     | 'varchar(255)' |
|     | `createdAt` | 'timestamp'    | CURRENT_TIMESTAMP |

**LEVEL UP =>**

- **organiser** l'arborescence du projet
- créer des **requêtes préparées**
- switcher de jQuery à **JS Vanilla**
