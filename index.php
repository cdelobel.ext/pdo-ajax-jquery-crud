<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TEST PHP & PDO</title>
    <style>
        span.id, .confirm_delete {
            display:none;
        }
        .confirm_delete {
            color:red;
            text-transform:uppercase;
            font-weight:bold;
        }
        .confirm_delete .show_user {
            color:#000;
            font-size: 1.2rem;
        }
        .confirm_delete button {
            color:#000;
        }
    </style>
</head>
<body>
  <h1>TEST PHP & PDO</h1>  
  <h2>S'inscrire - INSERT</h2>
  <form id="inscription" action="" method='POST'>
      <label for="pseudo">pseudo</label>
      <input type="text" name="pseudo" id="pseudo" placeholder="pseudo">
      <br>
      <label for="mdp">mot de passe</label>
      <input type="password" name="mdp" id="mdp" placeholder="mot de passe"><span class="view_password">voir le mot de passe</span>
      <br>
      <label for="email">email</label>
      <input type="email" name="email" id="email" placeholder="email">
      <br>
      <input type="submit" value="s'enregistrer">
  </form>
  <p class="error" style="color:red; font-weight:bold; text-transform:uppercase;"></p>
  <hr>
  <h2>Afficher les membres inscris - SELECT</h2>
  <button class="show_users">afficher les utilisateurs</button>
  <p class="list_users"></p>
  <hr>
  <h2>Supprimer un membre - DELETE</h2>
  <div class="list_delete">
    <div class="list_users_delete"></div>
    <p class="confirm_delete">Confirmer la suppression de l'utilisateur <span class="show_user"></span> ? <button type="button" class="confirm_delete">OUI</button></p>
  </div>  
  <hr>
  <h2>Modifier le pseudo - UPDATE</h2>
  <div class="list_update">
    <div class="list_users_update"></div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script>
     $(function() {
        console.log( "ready!" );
        $('.view_password').click(function(){
            console.log('Voir le mot de passe');

        });
        // FUNCTION AFFICHER LES UTILISATEURS
        function list_users_fn(){
            $.ajax({
                url : 'lecture.php',
                dataType : 'html',
                success : function(code, statut){
                    $('.list_users').html(code);
                }
            });
        }
        // FUNCTION AFFICHER LES UTILISATEURS + BUTTON DELETE
        function list_users_delete(){
            $.ajax({
                url : 'delete.php',
                dataType : 'html',
                success : function(code, statut){
                $('.list_users_delete').html(code);
            }
        });
        }

        // FUNCTION AFFICHER LES UTILISATEURS + BUTTON UPDATE
        function list_users_update(){
            $.ajax({
                url : 'update.php',
                dataType : 'html',
                success : function(code, statut){
                $('.list_users_update').html(code);
            }
        });
        }

        // ON AFFICHE LA LISTE DES UTILISATEURS + BUTTON UPDATE
        list_users_update();

        // ON AFFICHE LA LISTE DES UTILISATEURS + BUTTON DELETE
        list_users_delete();
        $('.confirm_delete').hide();

        // SUBMIT INSCRIPTION
        $('#inscription').submit(function(e){
            e.preventDefault();
            console.log('envoyer');
            if($('#inscription #pseudo').val() == '' || $('#inscription #mdp').val() == '' || $('#inscription #email').val() == '' ){
                $('.error').html('veuillez completer tous les champs !');
            }
            else {
                var user = $(this).serialize();
                $.ajax({
                    url :'inscription.php',
                    method : 'post',
                    data : user,
                    dataType : 'html',
                    success : function(code_html, statut){
                        if(code_html == '1'){
                            $('.error').show().html('Cette adresse email est déjà utilisée !'); 
                        }
                        else if(code_html == 'emailERROR'){
                            $('.error').show().html('Cette adresse email est invalide !'); 
                        }
                        else {
                        $('.error').show().css('color', 'green').html(code_html);
                        $('#inscription').slideUp();
                        $('.show_users').hide();
                        list_users_fn();
                        list_users_delete();
                        list_users_update();
                        }
                     }
                });
            }
            
        });        

        // BUTTON AFFICHER LES UTILSATEURS
        $('.show_users').click(function(){
            $(this).hide();
            list_users_fn();
        });

        
        // BUTTON DELETE UN UTILISATEUR
        $('.list_delete').on('click', '.delete', function(){
            $('.confirm_delete').show();
            console.log('Supression');
            var id = $(this).prev('.id').html();
            sessionStorage.setItem("local_id",id);
            var user = $(this).parent('.user').find('.pseudo').html();
            $('.show_user').html(user);
            console.log(id + ' et ' + user);          
        });
        $('.confirm_delete').click(function(){
                    var loc_id = sessionStorage.getItem("local_id");
                    $.ajax({
                    url : 'delete.php',
                    dataType : 'html',
                    method : 'post',
                    data : 'ID=' + loc_id,
                    success : function(code, statut){
                        $('.show_users').hide();
                        $('.list_users_delete').html(code);
                        $('.confirm_delete').hide();
                        list_users_fn();
                        $('.error').hide();
                        $('#inscription').slideDown();
                        list_users_update();
                    }
                });
            });
        
        // BUTTON UPDATE UN UTILISATEUR
        $('.list_update').on('click', '.update', function(){
            console.log('Modifier');
            var id = $(this).prev('.id').html();
            sessionStorage.setItem("local_id",id);
            var user = $(this).parent('.user').find('.pseudo').html();
            console.log(id + ' et ' + user);
            $input = '<input type="text" value="' + user + '">';
            $(this).parent('.user').find('.pseudo').html($input);
            $(this).removeClass('update').addClass('update_OK'); 
            $(this).text('mettre à jour');       
        });
        $('.list_update').on('click', '.update_OK', function(){
            console.log('Modifier_OK');
            var loc_id = sessionStorage.getItem("local_id");
            var pseudo = $('.pseudo input').val();
            console.log(pseudo + ' et ' + loc_id);
            $.ajax ({
                url : 'update.php',
                method : 'post',
                data : 'pseudo=' + pseudo +
                       '&ID=' + loc_id,
                success : function(code, statut){
                    list_users_fn();
                    list_users_delete();
                    list_users_update();
                    $('.error').hide();
                    $('#inscription').slideDown();
                }
            
            });
     
        });
        
    });
  </script>
</body>
</html>