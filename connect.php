<?php
$host = 'localhost';
$dbname = '_pdoTEST';
$utilisateur = 'root';
$mdp = 'root'; 
try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $utilisateur, $mdp);
    $conn->exec("SET CHARACTER SET utf8");
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
catch(PDOException $e)
    {
    echo $conn . "<br>" . $e->getMessage();
    }
?>